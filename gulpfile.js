/**
 *  @author Peter Ingram <peter.ingram0@gmail.com>
 */

'use strict';

/**
 * Declare dependencies
 */
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	bowerFiles = require('bower-files')(),
	concat = require('gulp-concat'),
	sass = require('gulp-sass'),
	minifycss = require('gulp-minify-css'),
	eslint = require('gulp-eslint'),
	ngHtml2Js = require("gulp-ng-html2js");

var bower = 'bower_components',
	target = 'dist',
	build = 'build';

gulp.task('vendor', function(){
	var files = bowerFiles.ext('js').files || [];
	return gulp.src( files )
		.pipe( concat('vendor.js') )
		.pipe( gulp.dest(target) );
});

gulp.task('scripts', function(){
	return gulp.src( build + '/**/*.js' )
		.pipe( concat('script.js') )
		.pipe( gulp.dest(target) );
});

gulp.task('html', function() {

	gulp.src('./build/index.html')
		.pipe(gulp.dest( target ));

	gulp.src("build/**/*.tpl.html")
		.pipe(ngHtml2Js({
			moduleName: "partials"
		}))
		.pipe(concat("partials.js"))
		.pipe(gulp.dest("./dist"));

});

gulp.task('styles', function() {
	return gulp.src('build/styles/styles.scss')
		.pipe(sass({
			includePaths: [
				bower + '/bootstrap-sass-official/assets/stylesheets',
				bower + '/sweetalert/dev'
			]
		}))
		.pipe(minifycss())
		.pipe(gulp.dest( target ))
});

/**
 * Runs the clean task first then runs everything needed to build up that target directory
 */
gulp.task('build', function() {
	gulp.start('vendor', 'scripts', 'html', 'styles');
});