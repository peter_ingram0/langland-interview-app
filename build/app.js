(function() {
	'use strict';

	/**
	 * Load in all the bower modules and my own app. modules
	 *
	 * @author Peter Ingram <peter.ingram0@gmail.com>
	 */
	angular
		.module('app', [
			'ngLodash',
			'ui.router',
			'firebase',
			'ui.bootstrap',
			'oitozero.ngSweetAlert',
			'partials',
			'app.routes',
			'app.controllers',
			'app.directive',
			'app.factory'
		]);

	/**
	 * Initialize the modules here
	 */
	angular.module('app.routes', ['ui.router']);
	angular.module('app.controllers', ['ngLodash', 'firebase', 'oitozero.ngSweetAlert']);
	angular.module('app.directive', []);
	angular.module('app.factory', []);

})();
