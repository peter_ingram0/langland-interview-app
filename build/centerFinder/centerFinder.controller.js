(function() {
	'use strict';

	angular
		.module('app.controllers')
		.controller('centerFinder', centerFinder);

	centerFinder.$inject = ['googleMap', '$firebaseArray', '$state', 'SweetAlert'];

	/**
	 * Center Finder Page
	 *
	 * @author Peter Ingram <peter.ingram0@gmail.com>
	 */
	function centerFinder(googleMap, $firebaseArray, $state, SweetAlert) {

		// Link to respondents endpoint
		var firebaseRespondents = new Firebase('https://survey.firebaseio.com/respondents');

		// Message que
		var firebaseQue = new Firebase('https://survey.firebaseio.com/zapier_queue/out');

		// Bind to VM
		var vm = this;

		// load the map
		var map = false;

		/**
		 * Init function
		 */
		vm.init = function() {

			// Pins
			vm.pins = [
				{address: 'Manchester Royal Infirmary, M13 9WL', position: [53.4616409, -2.2238154]},
				{address: 'St. James Hospital, Leeds, LS9 7TF', position: [53.8070052, -1.5212106]},
				{address: 'Queen’s Medical Centre, Nottingham, NG7 2UH', position: [52.9440787, -1.186186]},
				{address: 'Selly Oak Hospital, Birmingham, BS29 6JD', position: [52.4364143, -1.9343393]},
				{address: 'King’s College Hospital, London, SE5 9RS', position: [51.467917, -0.093512]},
				{address: 'Milton Keynes Hospital, Milton Keynes, MK6 5LD', position: [52.0263664, -0.735206]},
				{address: 'Southampton General Hospital, Southampton, SO16 6YD', position: [50.9338217, -1.433757]},
				{address: 'Bristol Royal Infirmary, Bristol, BS2 8HW', position: [51.4583559, -2.5966337]},
				{address: 'The Great Western Hospital, Swindon, SN3 6BB', position: [51.4957261, -1.9271336]},
				{address: 'John Radcliffe Hospital, Oxford, OX3 9DU', position: [51.7637365, -1.2179804]}
			];

		};

		// Init
		vm.init();

		/**
		 * Check the users have answered all the questions if they are on this page, if not send them back
		 */
		vm.checkQuestionsAnswered = function(validEntries) {

			// They have come directly to this page, lets send them away
			if(validEntries === undefined) {
				SweetAlert.swal('Oops...', 'Looks like you didnt answer the questions. Back you go', 'error');
				$state.go('wrapper.questions');
			}

		}

		/**
		 * Change postcode click function
		 */
		vm.changePostcode = function() {
			if(vm.form !== undefined && vm.form.postCode)
				changeAddress(vm.form.postCode);
			else
				SweetAlert.swal('Oops...', 'Please enter your postcode', 'error');
		};

		/**
		 * Submit form to firebase
		 */
		vm.submit = function() {

			var data = {
				postCode: vm.form.postCode,
				email: vm.form.email,
				closest: vm.closestPin.address,
				createdAt: Firebase.ServerValue.TIMESTAMP
			};

			/**
			 * Add the data to firebase then change the route
			 */
			$firebaseArray(firebaseRespondents)
				.$add(data)
				.then(addToMessageQue);

			/**
			 * Add a copy of this data to the message que. This will be deleted one an email has been sent
			 */
			function addToMessageQue() {
				$firebaseArray(firebaseQue)
					.$add(data)
					.then(success)
			}

			/**
			 * Once the record has been added in Firebase lets send them to a thank you page
			 */
			function success() {
				$state.go('wrapper.thankYouSuccess');
			}

		};

		/**
		 * Init google map, changes the var
		 */
		function initialize(elementId, center) {
			map = googleMap.initialize(elementId, center, vm.pins);
		}

		/**
		 * Change the address of the map by typing into the input box. This will check the response of the request
		 * then reBind the center and pan the map
		 */
		function changeAddress(address) {

			googleMap.postcodeToLongLat(address)
				.then(checkResponse)
				.then(checkInstanceExists)
				.then(findClosestPin)
				.then(panMap);

			/**
			 * Check google's response is ok
			 */
			function checkResponse(response) {

				// Check the response
				if(response.status !== 200 || response.data.results.length === 0) {
					return false;
				} else
					return response;

			}

			/**
			 * Ensure that an instance of map exists, if it doesn't create it & pass on the center geometry
			 */
			function checkInstanceExists(response) {

				if(!response)
					return;

				var center = [
					response.data.results[0].geometry.location.lat,
					response.data.results[0].geometry.location.lng
				];

				if(!map)
					initialize('map', center);

				return center;

			}

			/**
			 * Find the closest pin to the new location
			 */
			function findClosestPin(center) {

				if(!center)
					return;

				vm.closestPin = googleMap.closestPinToMe(center[0], center[1], vm.pins);

				return center;

			}

			/**
			 * Pan the map to the new location
			 */
			function panMap(center) {

				if(!center) {
					SweetAlert.swal('Oops...', 'No locations found, please try again', 'error');
					vm.form.postCode = '';
					return;
				}

				var LatLng = new google.maps.LatLng(center[0], center[1]);

				if(map)
					map.panTo(LatLng);

			}

		}

	}

})();
