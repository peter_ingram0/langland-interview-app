(function() {
	'use strict';

	angular
		.module('app.controllers')
		.controller('wrapper', wrapper);

	wrapper.$inject = ['$state', 'lodash'];

	/**
	 * Main wrapper controller surrounds the application
	 *
	 * @author Peter Ingram <peter.ingram0@gmail.com>
	 */
	function wrapper($state, lodash) {

		// Bind the view
		var vm = this;

		/**
		 * Questions list with ID's and text. Answers get injected into this array
		 * @type {string[]}
		 */
		vm.questions = [
			{id: 1, text: 'Question 1 text'},
			{id: 2, text: 'Question 2 text'},
			{id: 3, text: 'Question 3 text'},
			{id: 4, text: 'Question 4 text'},
			{id: 5, text: 'Question 5 text'},
			{id: 6, text: 'Question 6 text'},
			{id: 7, text: 'Question 7 text'},
			{id: 8, text: 'Question 8 text'},
			{id: 9, text: 'Question 9 text'},
			{id: 10, text: 'Question 10 text'}
		];

		/**
		 * Form submit function
		 */
		vm.submit = function() {

			// Build up array
			var criteria = [];

			/**
			 * Check each answer against the rules provided, create a pass array of true and false
			 * based on these rules
			 */
			angular.forEach(vm.questions, function(value) {
				criteria.push(checkAnswers(value));
			});

			// Check the array for any false values
			vm.criteriaResult = ! lodash.includes(criteria, false);

			// If they meet the criteria lets send them to center finder, else send them to the thank you page
			if(vm.criteriaResult) {
				$state.go('wrapper.centerFinder');
			} else {
				$state.go('wrapper.thankYou');
			}

		}

		/**
		 * Check the answers provided against the rules
		 */
		function checkAnswers(question) {

			if(question.id <= 4 && question.answer === 'yes') {
				return true;
			}

			else if(question.id > 4 && question.answer === 'no') {
				return true;
			}

			else {
				return false;
			}

		}

	}

})();