(function() {
	'use strict';

	angular
		.module('app.controllers')
		.controller('authModal', authModal);

	authModal.$inject = ['$rootScope', '$scope', '$modalInstance', 'auth', 'SweetAlert'];

	/**
	 * Handles modal interactions
	 *
	 * @author Peter Ingram <peter.ingram0@gmail.com>
	 */
	function authModal($rootScope, $scope, $modalInstance, auth, SweetAlert) {

		/**
		 * Login
		 */
		$scope.login = function() {

			if( ! $scope.form.email && ! $scope.form.password)
				return;

			/**
			 * Auth
			 */
			auth.firebaseAuth($scope.form.email, $scope.form.password)
				.then(success)
				.catch(fail);

			/**
			 * Will run a successful login attempt
			 */
			function success() {
				$modalInstance.dismiss('cancel');
				$rootScope.$broadcast('login:success');
			}

			/**
			 * Failed login attempt
			 */
			function fail(response) {
				SweetAlert.swal('Oops...', response, 'error');
				$scope.form.email = '';
				$scope.form.password = '';
			}

		};

	}

})();