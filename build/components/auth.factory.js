(function() {
	'use strict';

	angular
		.module('app.factory')
		.factory('auth', auth);

	auth.$inject = ['$firebaseAuth', '$q'];

	/**
	 * Handles authentication for firebase
	 *
	 * @param $firebaseAuth
	 * @returns {{firebaseAuth: firebaseAuth}}
	 *
	 * @author Peter Ingram <peter.ingram0@gmail.com>
	 */
	function auth($firebaseAuth, $q) {

		var service = {
			firebaseAuth: firebaseAuth
		};

		return service;

		/**
		 * Login function, has an internal promise as well as its own promise
		 *
		 * @param email
		 * @param pass
		 */
		function firebaseAuth(email, pass) {

			// Setup our promise
			var deferred = $q.defer();

			// Firebase ref
			var ref = new Firebase('https://survey.firebaseio.com');

			/**
			 * Try and auth then run success or fail
			 */
			$firebaseAuth(ref).$authWithPassword({email: email, password: pass})
				.then(success)
				.catch(fail);

			/**
			 * Successful login attempt
			 */
			function success(authData) {
				deferred.resolve(authData);
			}

			/**
			 * Failed login attempt
			 */
			function fail(error) {
				deferred.reject(error.message);
			}

			// Return our promise
			return deferred.promise;

		}

	}

})();