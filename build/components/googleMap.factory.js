(function() {
	'use strict';

	angular
		.module('app.factory')
		.factory('googleMap', googleMap);

	googleMap.$inject = ['$http', 'lodash'];

	/**
	 * Factory to control google maps
	 *
	 * @author Peter Ingram <peter.ingram0@gmail.com>
	 */
	function googleMap($http, lodash) {

		var service = {
			initialize: initialize,
			getDistance: getDistance,
			postcodeToLongLat: postcodeToLongLat,
			closestPinToMe: closestPinToMe
		};

		return service;

		/**
		 * Function sets up google maps with the element ID, the center array and the pins objects
		 * returns an instance of google map to use
		 *
		 * @param elementId
		 * @param center Array
		 * @param pins Object
		 *
		 * @returns {google.maps.Map}
		 */
		function initialize(elementId, center, pins) {

			/**
			 * Setup the map options from the center provided
			 */
			var mapOptions = {
				center: {
					lat: center[0],
					lng: center[1]
				},
				zoom: 9,
				scrollwheel: false,
				streetViewControl: 0,
				overviewMapControl: 0,
				mapTypeControl: 0
			};

			// Create the map instance
			var map = new google.maps.Map(document.getElementById(elementId), mapOptions);

			/**
			 * Add pin for our location
			 */
			addPin(true, 'You are here', center[0], center[1], map);

			/**
			 * Add each of the locations to the map
			 */
			if(pins.length > 0) {
				angular.forEach(pins, function(pin) {
					addPin(false, pin.address, pin.position[0], pin.position[1], map);
				});
			}

			return map;

		}

		/**
		 * If an object of pins is provided this function takes its arguments and appends to the map
		 *
		 * @param info
		 * @param positionLng
		 * @param positionLat
		 * @param map
		 */
		function addPin(openWindow, info, positionLng, positionLat, map) {

			var contentString =
				'<div id="content">' +
				'<div id="bodyContent">' +
				'<p>' + info + '</p>' +
				'</div>' +
				'</div>';

			var position = new google.maps.LatLng(positionLng, positionLat);

			var marker = new google.maps.Marker({
				position: position,
				map: map,
				title: info
			});

			var infowindow = new google.maps.InfoWindow({
				content: contentString,
				maxWidth: 250
			});

			if(openWindow) {
				infowindow.open(map, marker);
			} else {
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map, marker);
				});
			}

		}

		/**
		 * Pass in two locations and this function will return the distance in K between the two locations
		 *
		 * @param lat1
		 * @param lon1
		 * @param lat2
		 * @param lon2
		 * @returns {number}
		 */
		function getDistance(lat1,lon1,lat2,lon2) {
			var R = 6371; // Radius of the earth in km
			var dLat = deg2rad(lat2-lat1);  // deg2rad below
			var dLon = deg2rad(lon2-lon1);
			var a =
					Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
					Math.sin(dLon/2) * Math.sin(dLon/2)
				;
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c; // Distance in km
			return d;
		}

		/**
		 * Helper for getDistanceFromLatLonInKm
		 */
		function deg2rad(deg) {
			return deg * (Math.PI/180)
		}

		/**
		 * Calls the google maps API to get full address details
		 *
		 * @param address
		 * @returns {*}
		 */
		function postcodeToLongLat(address) {
			return $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address)
		}

		/**
		 * Pass in your lat and lon and all pins and this function will return you
		 * the pin object that's the closest to you
		 *
		 * @param myLat
		 * @param myLon
		 * @param allPins
		 */
		function closestPinToMe(myLat, myLon, allPins) {

			var distance = [];

			angular.forEach(allPins, function(pin) {
				distance.push(getDistance(myLat, myLon, pin.position[0], pin.position[1]));
			});

			var closestKey = lodash.indexOf(distance,
				lodash.min(distance)
			);

			return allPins[closestKey];

		}


	}

})();