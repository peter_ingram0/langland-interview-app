(function() {
	'use strict';

	angular
		.module('app.directive')
		.directive('questionRadio', questionRadio);

	/**
	 * Provides a radio directive, pass in the question object which has an ID and text
	 *
	 * @author Peter Ingram <peter.ingram0@gmail.com>
	 */
	function questionRadio() {
		
		var directive = {
			scope: {
				question: '=' // object with ID and Text
			},
			templateUrl: 'components/questionRadio/questionRadio.tpl.html',
			restrict: 'EA'
		};

		return directive;

	}

})();