(function() {
	'use strict';

	angular
		.module('app.controllers')
		.controller('respondentReport', respondentReport);

	respondentReport.$inject = ['$scope', '$rootScope', '$modal', '$firebaseObject'];

	/**
	 * Admin page requires login, will display all the respondents and allow the user to mark as
	 * contacted for each one
	 *
	 * @author Peter Ingram <peter.ingram0@gmail.com>
	 */
	function respondentReport($scope, $rootScope, $modal, $firebaseObject) {

		// Bind controller to vm
		var vm = this;

		// Ref
		var ref = new Firebase('https://survey.firebaseio.com/respondents');

		/**
		 * Mark the contact as contacted
		 */
		vm.markContacted = function(id) {
			updateRespondent(id);
		};

		/**
		 * open Modal with settings
		 */
		$modal.open({
			animation: true,
			templateUrl: 'myModalContent.html',
			size: 'sm',
			controller: 'authModal',
			keyboard: false,
			backdrop: 'static'
		});

		/**
		 * On Broadcast of login try and get the respondents
		 */
		$rootScope.$on('login:success', function() {
			getRespondents()
		});

		/**
		 * We are now logged in, lets go get the data required
		 */
		function getRespondents() {
			var syncObject = $firebaseObject(ref);
			syncObject.$bindTo($scope, 'respondents');
		}

		/**
		 * Update the respondent as contacted
		 *
		 * @param firebaseID
		 */
		function updateRespondent(firebaseID) {

			if( ! $scope.respondents[firebaseID] )
				return;

			$scope.respondents[firebaseID].contacted = true;

		}

	}

})();