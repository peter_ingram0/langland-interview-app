(function() {
	'use strict';

	angular
		.module('app.routes')
		.config(Routing);

	Routing.$inject = ['$stateProvider', '$urlRouterProvider'];

	/**
	 * This module controllers all the routing in the application. All routing going though the
	 * wrapper. The wrapper can provide some global functionality to the application and render
	 * a global header, footer.
	 *
	 * @param $stateProvider
	 * @param $locationProvider
	 * @param $urlRouterProvider
	 * @constructor
	 */
	function Routing($stateProvider, $urlRouterProvider) {

		// Default location
		$urlRouterProvider.otherwise("/")

		/**
		 * Main wrapper provides a header / footer on the application, alll views get
		 * loaded into the wrapper.tpl.html template
		 */
		$stateProvider.state('wrapper', {
			controller: 'wrapper as wrapper',
			templateUrl: 'wrapper/wrapper.tpl.html'
		});

		$stateProvider.state('wrapper.questions', {
			url: '/',
			templateUrl: 'questions/questions.tpl.html',
		});

		$stateProvider.state('wrapper.centerFinder', {
			url: '/center-finder',
			controller: 'centerFinder as centerFinder',
			templateUrl: 'centerFinder/centerFinder.tpl.html',
		});

		$stateProvider.state('wrapper.thankYou', {
			url: '/thank-you',
			templateUrl: 'thankYou/thankYou.tpl.html',
		});

		$stateProvider.state('wrapper.thankYouSuccess', {
			url: '/thank-you-success',
			templateUrl: 'thankYou/thankYouSuccess.tpl.html',
		});

		$stateProvider.state('wrapper.respondentReport', {
			url: '/respondent-report',
			controller: 'respondentReport as respondentReport',
			templateUrl: 'respondentReport/respondentReport.tpl.html',
		});

	}

})();